#### Project 1 Requirements
1. code sql tables
2. person table must have 15 records, the rest have 5 records per table
3. run sql reports

#### ERD Screenshot
![tables](img/erd.png)

#### Person Table
![person](img/person.png)

#### Report1
| code | screenshot |
| ------ | ------ |
| [![r1Code](img/code1.png)](img/code1.png) | [![r1Image](img/img1.png)](img/img1.png) |

#### ERD FILE
[p1.mwb file](p1.mwb)
