#### Assignment 4 Requirements
1. code sql tables
2. person table must have 10 records, the rest can have 5 records per table
3. run sql reports
4. must use sql server

#### ERD Screenshot
![tables](img/diagram.png)

#### Person Table
![person](img/personTable.png)

#### Report1
| code | screenshot |
| ------ | ------ |
| [![r1Code](img/code1.png)](img/code1.png) | [![r1Image](img/img1.png)](img/img1.png) |


