# LIS3781
## Jared Jaskolski
### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - provide screenshots of installations
    - create Bitbucket repo
    - complete Bitbucket tutorials
    - provide git command descriptions
2. [A2 README.md]( a2/README.md "My A2 README.md file")
    - tables and insert statements
    - include indexes and foreign keys
    - include query result sets
    - populate at least 5 records
3. [A3 README.md]( a3/README.md "My A3 README.md file")
    - create tables in Oracle
    - provide table screenshots
    - run extra requirements 
4. [A4 README.md]( a4/README.md "My A4 README.md file")
    - create tables in SQL Server
    - run reports
    - person table screenshot
5. [A5 README.md]( a5/README.md "My A5 README.md file")
    - create tables in SQL Server
    - run reports
    - sale table screenshot
6. [P1 README.md]( p1/README.md "My A1 README.md file")
    - code sql tables
    - person table must have 15 records, the rest have 5 records per table
    - run sql reports
    - provide screenshots
7. [P2 README.md]( p2/README.md "My A1 README.md file")
    - download mongo
    - import database
    - run reports
    - run mongo command