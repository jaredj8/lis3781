#### Assignment 2 Requirements
1. create tables in Oracle
2. provide table screenshots
3. run extra requirements

#### SQL Screenshots
| codeA | codeB |
| ------ | ------ |
| [![company](img/codeA.png)](img/codeA.png) | [![customer](img/codeB.png)](img/codeB.png) |

#### Populated Tables
![tables](img/tables.png)

#### Q1 Screenshot
[![1](img/1.png)](img/1.png)

#### Q2 Screenshot
![2](img/2.png)

#### Q3 Screenshot
![3](img/3.png)

#### Q4 Screenshot
[![4](img/4.png)](img/4.png)

#### Q5 Screenshot
[![5](img/5.png)](img/5.png)

#### Q6 Screenshot
[![6](img/6.png)](img/6.png)

#### Q7 Screenshot
[![7](img/7.png)](img/7.png)

#### Q8 Screenshot
![8](img/8.png)

#### Q9 Screenshot
![9](img/9.png)

#### Q10 Screenshot
![10](img/10.png)