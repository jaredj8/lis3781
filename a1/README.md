#### Assignment 1 Requirements
1. Distributed Version Control with Git and Bitbucket
2. AMPPS Install
3. Questions
4. ERD and Code
5. Repo links

#### Git commands w/ short descriptions
1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List, create, or delete branches

#### AMPPS Install
![ampps](img/ampps.png)

#### ERD 
![erd](img/erd.png)

#### Ex1 Screenshot
![e1](img/e1.png)

#### Ex2 Screenshot
![e2](img/e2.png)

#### Ex3 Screenshot
![e3](img/e3.png)

#### Ex4 Screenshot
![e4](img/e4.png)

#### Ex5 Screenshot
![e5](img/e5.png)

#### Ex6 Screenshot
![e6](img/e6.png)

#### Ex7 Screenshot
![e7](img/e7.png)

#### Bitbucket Station Locations
[Bitbucket Station Locations](https://bitbucket.org/jaredj8/bitbucketstationlocations/)