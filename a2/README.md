#### Assignment 2 Requirements
1. tables and insert statements
2. include indexes and foreign keys
3. include query result sets
4. populate at least 5 records

#### SQL Screenshots
| Company | Customer |
| ------ | ------ |
| [![company](img/codeA.png)](img/codeA.png) | [![customer](img/codeB.png)](img/codeB.png) |

#### Populated Tables
![tables](img/tables.png)

#### Q3 Screenshot
| 3a | 3b | 3c |
| ------ | ------ | ------ |
| [![3a](img/3a.png)](img/3a.png) | [![3b](img/3b.png)](img/3b.png) | [![3c](img/3c.png)](img/3c.png) |

#### Q4 Screenshot
![4](img/4.png)

#### Q5 Screenshot
![5](img/5.png)

#### Q6 Screenshot
| 6a | 6b |
| ------ | ------ |
| [![6a](img/6a.png)](img/6a.png) | [![6b](img/6b.png)](img/6b.png) |

#### Q7 Screenshot
| 7a | 7b |
| ------ | ------ |
| [![7a](img/7a.png)](img/7a.png) | [![7b](img/7b.png)](img/7b.png) |

#### Q8 Screenshot
| 8a | 8b |
| ------ | ------ |
| [![8a](img/8a.png)](img/8a.png) | [![8b](img/8b.png)](img/8b.png) |

#### Q9 Screenshot
| 9a | 9b |
| ------ | ------ |
| [![9a](img/9a.png)](img/9a.png) | [![9b](img/9b.png)](img/9b.png) |

#### Q10 Screenshot
![10](img/10.png)