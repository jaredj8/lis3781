drop database if exists jj19v;
create database if not exists jj19v;
use jj19v;

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
	cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
	cmp_street VARCHAR(30) NOT NULL,
	cmp_city VARCHAR(30) NOT NULL,
	cmp_state CHAR(2) NOT NULL,
	cmp_zip INT(9) UNSIGNED ZEROFILL NOT NULL COMMENT 'no dashes in zip',
	cmp_phone BIGINT UNSIGNED NOT NULL COMMENT 'ssn and zip can be zero filled',
	cmp_ytd_sales DECIMAL(10,2) UNSIGNED NOT NULL COMMENT '12,345,678.90',
	cmp_email VARCHAR(100) NULL,
	cmp_url VARCHAR(100) NULL,
	cmp_notes VARCHAR(255) NULL,
	PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'C-Corp','507 - 20th Ave. E. Apt. 2A','Seattle','WA','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'S-Corp','Vermilion Way','Mays Landing','NJ','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'Non-Profit-Corp','Lower Way','Tuscaloosa','AL','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'LLC','Chapel Lane','Largo','FL','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'Partnership','Earl Route','Bowie','MD','081226749','2065559857','1234567.00',null,'google.com',null);