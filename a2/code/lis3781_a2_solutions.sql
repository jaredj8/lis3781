drop database if exists jj19v;
create database if not exists jj19v;
use jj19v;

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
	cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
	cmp_street VARCHAR(30) NOT NULL,
	cmp_city VARCHAR(30) NOT NULL,
	cmp_state CHAR(2) NOT NULL,
	cmp_zip INT(9) UNSIGNED ZEROFILL NOT NULL COMMENT 'no dashes in zip',
	cmp_phone BIGINT UNSIGNED NOT NULL COMMENT 'ssn and zip can be zero filled',
	cmp_ytd_sales DECIMAL(10,2) UNSIGNED NOT NULL COMMENT '12,345,678.90',
	cmp_email VARCHAR(100) NULL,
	cmp_url VARCHAR(100) NULL,
	cmp_notes VARCHAR(255) NULL,
	PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'C-Corp','507 - 20th Ave. E. Apt. 2A','Seattle','WA','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'S-Corp','Vermilion Way','Mays Landing','NJ','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'Non-Profit-Corp','Lower Way','Tuscaloosa','AL','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'LLC','Chapel Lane','Largo','FL','081226749','2065559857','1234567.00',null,'google.com',null),
(null,'Partnership','Earl Route','Bowie','MD','081226749','2065559857','1234567.00',null,'google.com',null);

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
	cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	cmp_id INT UNSIGNED NOT NULL,
	cus_ssn binary(64) NOT NULL,
	cus_salt binary(64) NOT NULL,
	cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
	cus_first VARCHAR(15) NOT NULL,
	cus_last VARCHAR(30) NOT NULL,
	cus_street VARCHAR(30) NULL,
	cus_city VARCHAR(30) NULL,
	cus_state CHAR(2) NULL,
	cus_zip INT(9) UNSIGNED ZEROFILL NULL,
	cus_phone BIGINT UNSIGNED NOT NULL,
	cus_email VARCHAR(100) NULL,
	cus_balance DECIMAL(7,2) UNSIGNED NULL,
	cus_tot_sales DECIMAL(7,2) UNSIGNED NULL,
	cus_notes VARCHAR(255) NULL,
	PRIMARY KEY (cus_id),

	UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
	INDEX idx_cmp_id (cmp_id ASC),

	CONSTRAINT fk_customer_company
		FOREIGN KEY (cmp_id)
		REFERENCES company (cmp_id)
		ON DELETE NO ACTION
		ON UPDATE CASCADE
)

ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt,000456789),512)),@salt,'Discount','Wilbur','Denaway','23 Billings Gate','El Paso','TX','085703412','2145559857','test1@gmail.com','8391.87','37642.00',null),
(null,4,unhex(SHA2(CONCAT(@salt,001456789),512)),@salt,'Loyal','Bradford','Casis','995 Bedford Road','Venice','FL','085703412','2145559857','test1@gmail.com','8391.87','37642.00',null),
(null,3,unhex(SHA2(CONCAT(@salt,002456789),512)),@salt,'Impulse','Valerie','Lieblong','327 Bear Hill Lane','Romulus','MI','085703412','2145559857','test1@gmail.com','8391.87','37642.00',null),
(null,5,unhex(SHA2(CONCAT(@salt,003456789),512)),@salt,'Need-Based','Kathy','Jeffries','9 Marshall Street','New York','NY','085703412','2145559857','test1@gmail.com','8391.87','37642.00',null),
(null,1,unhex(SHA2(CONCAT(@salt,004456789),512)),@salt,'Wandering','Steve','Rogers','73 Walnutwood Lane','Amsterdam','NY','085703412','2145559857','test1@gmail.com','8391.87','37642.00',null);